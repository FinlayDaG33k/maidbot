# MaidBot

Discord bot for Das Propaganda Maschine :)  
Should not be confused with an [age-old project](https://github.com/finlaydag33k/maidbot) of mine that is also called "MaidBot".  

# Dependencies
- Yarn

# TODO
- [ ] Make roles be addable by means of a reaction on a certain message
- [ ] Get a user's favourite channel
- [x] Get a user's first-seen
- [x] Get a user's last seen
- [x] Scan all channels for messages by the user
- [x] Store a count of all messages sent by a user
- [ ] Assign roles to users based on how much messages they have sent
- [x] Allow a user to look up profiles of others
- [ ] Store the first seen in database (for when a user has to rejoin quickly)
- [ ] Make scanning the message counts of users more efficient (especially for if the server grows)

# Required Permissions
Below a list of permissions MaidBot requires in order to properly serve:
- Manage Roles
- Send Messages
- Embed Links
- Read Message History
- Mention Everyone