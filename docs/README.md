# MaidBot
Welcome to my documentation Master.  
Here, you can read about what I can do and how it works.  
If something is wrong or missing, please head over to [my project page](https://gitlab.com/FinlayDaG33k/maidbot/) to fix it or let Teitoku know in [Das Propaganda Maschine](https://discord.gg/jWCeN77)!

# Usage of a command
Each command expects the prefix `!` (eg. `!reddit`) in order for me to execute it.  
On the page of each command, you can find more details on what a command does, what arguments I expect and some examples on how to use the commands.