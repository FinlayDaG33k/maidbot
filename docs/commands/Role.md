# Role
This command allows you to assign yourself to public roles.  
These roles serve no function but fun or organization.  
Please refrain yourself from adding too much unneeded tags master.

# Usage
```
!role <role>
```

# Arguments
| Argument | Optional | Comment |
|----------|----------|---------|
| role | false | The name of the role you want to assign yourself to |

# List of public roles
Below is a list of public roles Teitoku has made available for us to use:

| Role | Comment |
|----------|----------|
| VOC | For Dutch-speaking members |

# Example usage
```
!role VOC
```