# Minecraft
This command will allow you to interact with our Minecraft server.  
This server can be found on `mc.finlaydag33k.nl:25565`.  
A DynMap is also available [here](https://mc.finlaydag33k.nl/).

# Usage
```
!minecraft <subcommand> [OPTIONS]
```

# Subcommands
| Argument  | Comment                                   |
|-----------|-------------------------------------------|
| whitelist | Add a user to the whitelist on the server |