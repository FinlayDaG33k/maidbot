# Profile
This command will show you the top 10 users on this server by scores including some details such as:  
- The user's reputation score
- The user's total message count
- When someone joined the server
- The user's total score

Please refer to the [Profile](commands/Profile.md) page to learn more about how scores are calculated.

# Usage
```
!leaderboard
```