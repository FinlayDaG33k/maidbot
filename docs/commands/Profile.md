# Profile
This command will allow you to look up some statistics I've been keeping track of, such as:
- The first time someone joined the server
- The last time someone has spoken in the server
- The user's reputation score
- The user's total message count
- The user's total score
- The user's giveaway power

# Usage
```
!profile <@username>
```

# Arguments
| Argument | Optional | Comment |
|----------|----------|---------|
| @username | false | The user whose profile you want to see |

# Example Usage
```
!profile @MaidBot
```

# Score Calculation
I calculate your score based on some very simple metrics.  
Teitoku wants to make it a bit more advanced later down the road but he can't find a working formula that satisfies all his needs, so this will have to do for now:  
- 1 point per 5 messages
- 1 point per reputation point
- 1 point per 7 days you are a member of the server