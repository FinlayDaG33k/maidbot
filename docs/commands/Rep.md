# Rep
This command will allow you to upvote or downvote a specific user for a specific reason.  
Please do not misuse this command to misrepresent a specific user.  
When Teitoku finds out about this, he might issue a penalty for it.  

# Usage
```
!rep <@username> <rep> <message>
```

# Arguments
| Argument | Optional | Comment |
|----------|----------|---------|
| @username | false | A mention of the user you want to vote for |
| rep | false | `+` for an upvote or `-` for a downvote |
| message | true | An optional message on why you made this vote |

# Example Usage
```
!rep @MaidBot + Does her best to serve
```