FROM node:12

# Create our app dir
RUN mkdir /app

# Add everything needed
ADD dist /app/dist
ADD node_modules /app/node_modules
ADD docs /app/docs
ADD package.json /app/package.json

# Set the startup
WORKDIR /app
CMD ["yarn", "run", "start:prod"]