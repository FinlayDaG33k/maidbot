/**
 * Calculate someone's giveaway power by means of their score
 * Just run it through a log function and truncate the decimals
 * 
 * @param score 
 * @returns number
 */
export function giveawayPower(score: number): number {
  // Run the score through a log
  let power = Math.log(score);

  // Truncate the decimals
  power = Math.trunc(power);
  
  // Return the power
  return power;
}