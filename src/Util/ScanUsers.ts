import { Client, Collection, GuildMember, Guild } from "discord.js";
import { UsersModel } from '../Model/UsersModel';
import { Configure } from '../Configure';

export class ScanUsers {
  private _client: Client;

  public constructor(client: Client) {
    this._client = client;
  }

  /**
   * Will scan all users and add them to the UsersModel via ScanUsers#registerusers
   */
  public async scan() {
    // Find out guild
    let guild = await this._client.guilds.cache.find((guild: Guild) => guild.id == Configure.read("guild"));
    if(!guild) throw new Error("Guild not found!");
    if(!guild.members) throw new Error("Members not found!");

    let users = guild.members.cache;

    // Register all the users if they aren't yet
    await this.registerUsers(users);

    return;
  }

  /**
   * Will insert new users into the UsersModel if they aren't so yet
   * 
   * @param users 
   */
  private async registerUsers(users: Collection<string, GuildMember>) {
    // Loop through all users
    // Add them to the database if they aren't in there yet
    let promises = [];
    for(let user of users) {
      // Create a promise so we can handle multiple at one
      let promise = new Promise(async (resolve, reject) => {
        // Find a possible existing user
        let entry = await UsersModel.findOne({
          where: {
            uid: user[1].id
          }
        })

        // Check if we have a result
        // Otherwise, register the user
        if(!entry) {
          await UsersModel.create({
            uid: user[1].id,
            nickname: user[1].user.username
          })
        }

        // Resolve our promise
        resolve();
      });

      // Add the promise to our promises array
      promises.push(promise);
    }

    // Wait until we have scanned all users
    await Promise.all(promises);
  }
}