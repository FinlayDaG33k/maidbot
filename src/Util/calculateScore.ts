import { MessagesModel } from '../Model/MessagesModel';

/**
 * Calculate the score of a given the reputation, messages and time in the server
 * - 1 point per 5 messages
 * - 1 point per rep
 * - 1 point per 7 days in the server
 * 
 * @param rep 
 * @param messages 
 */
export function calculateScore(rep: number, messages: number, membershipDays: number): number {
  // Set our score to 0
  let score: number = 0;

  // Divide our messages by 5
  // Add this to the score
  score += messages / 5

  // Divide our rep by 2
  // Add this to the score
  score += rep

  // Get the days in the server
  // Add this to the score
  score += membershipDays / 7;

  // Truncate the decimals
  score = Math.trunc(score);

  // Return our final score
  return score;
}