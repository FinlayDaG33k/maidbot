import { MaidBot } from "main";
import { Client, Guild, GuildChannel, TextChannel, Collection, Message, Snowflake } from "discord.js";
import { Configure } from "../Configure";
import { MessagesModel } from '../Model/MessagesModel';

interface fetchOptions {
  limit?: any;
  before?: any;
}

export class MessageCount {
  private _counts: any = {}; 
  private _excluded: any[string] = [
    '694239722007953448'
  ];

  public async scan(client: Client) {
    console.log("Starting the scan of all channels...");

    // Get the server info
    let guild = client.guilds.cache.find((guild: Guild) => guild.id == Configure.read("guild"));
    if(!guild) return;

    // Get all the non-excluded text channels
    let channels = guild.channels.cache.filter((channel: GuildChannel) => {
      // Do not include channels that should be excluded
      if(this._excluded.includes(channel.id)) return false;

      // Include all text channels
      return channel.type == 'text';
    });
    console.log(`Found ${channels.size} channels...`);

    // Scan all the messages
    for(let entry of channels) {
      await new Promise(async (resolve, reject) => {
        // Cast the channel to something we can use
        let channel = entry[1] as TextChannel;

        // Fetch all messages
        let messages = await this.fetchAllMessages(channel);

        // Process the messages
        await this.processMessages(messages);

        resolve();
      });
    }
    console.log(`Found ${Object.keys(this._counts).length} users...`);

    // Update the count in the database
    for(let user in this._counts) {
      await new Promise(async (resolve, reject) => {
        // Find any existing entry for this user
        let entry = await MessagesModel.findOne({
          where: {
            uid: user
          }
        });

        // Update the entry or insert a new one
        if(entry) {
          // Entry found
          // Update it
          await entry.update({
            uid: user,
            data: JSON.stringify(this._counts[user])
          });
        } else {
          // No entry found
          // Insert it
          await MessagesModel.create({
            uid: user,
            count: JSON.stringify(this._counts[user])
          });
        }
        resolve();
      });
    }

    console.log(`Message counts are all synced up!`);
  }

  public async increase(uid: string) {
    let entry = await MessagesModel.findOne({
      where: {
        uid: uid
      }
    });

    if(entry) {
      // Entry found
      // Increase the counter
      let count = <number>entry.get("count");
      count++;

      // Update the record
      entry.update({
        count: count
      });

      return;
    } else {
      await MessagesModel.create({
        uid: uid,
        count: 1
      });
    }
  }

  private async processMessages(messages: Message[]) {
    messages.forEach((message: Message) => {
      // Check if we've already encountered this user
      if(!this._counts.hasOwnProperty(message.author.id)) this._counts[message.author.id] = {
        total: 0,
        channels: {}
      };

      // Check if we've already encountered this channel
      if(!this._counts[message.author.id]['channels'].hasOwnProperty(message.channel.id)) this._counts[message.author.id]['channels'][message.channel.id] = 0;

      // Update the counts
      this._counts[message.author.id]['total']++;
      this._counts[message.author.id]['channels'][message.channel.id]++;
    });
  }

  private async fetchAllMessages(channel: TextChannel) {
    const sum_messages = [];
    let last_id;

    while (true) {
      const options: fetchOptions = { limit: 100 };
      if (last_id) {
        options['before'] = last_id;
      }

      const messages = await channel.messages.fetch(options);
      sum_messages.push(...messages.array());
      last_id = messages.last()?.id;
      if(messages.size != 100) break;
    }

    return sum_messages;
  }
}