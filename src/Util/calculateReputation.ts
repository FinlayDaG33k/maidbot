import { ReputationsModel } from '../Model/ReputationsModel';

export function calculateReputation(data: ReputationsModel[]) {
  // Our score
  let score: number = 0;
  let upvotes: number = 0;

  // Loop over each entry
  data.forEach((entry) => {
    switch(entry.get('rep')) {
      case '+':
        score++;
        upvotes++;
        break;
      case '-':
        score--;
        break;
    }
  });

  return {
    score: score,
    upvotes: upvotes,
    total: data.length
  };
}