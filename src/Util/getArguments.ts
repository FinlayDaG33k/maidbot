export function getArguments(message: string, limit: number = 3) {
  let tokens = message.split(" ");
  if(tokens.length > limit) {
    var ret = tokens.splice(0, limit);
    ret.push(tokens.join(" "));
    return ret;
  }

  return tokens;
}