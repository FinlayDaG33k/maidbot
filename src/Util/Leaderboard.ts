import { Client, Guild, GuildMember } from "discord.js";

import { UsersModel } from '../Model/UsersModel';
import { ReputationsModel } from '../Model/ReputationsModel';
import { MessagesModel } from '../Model/MessagesModel';

import { calculateReputation } from '../Util/calculateReputation';
import { calculateScore } from '../Util/calculateScore';

import * as moment from 'moment';
import { Configure } from '../Configure';

export class Leaderboard {
  private static _leaderboard: any = {
    boards: [],
    updatedAt: null
  };
  private _client: Client;
  private _guild: Guild | undefined;
  private _isUpdating: boolean = false;
  private _excluded: any[string] = [
    '91616138860978176'
  ];

  public constructor(client: Client) {
    this._client = client;
  }

  public async update() {
    console.log('Updating Leaderboards');

    // Create a variable for our updated leaderboard
    // This way we don't have to change our old board until done
    let updatedLeaderboard: any = {boards: [], updatedAt: null};

    // Get the guild
    this._guild = await this._client.guilds.cache.find((guild: Guild) => guild.id == Configure.read("guild"));
    if(!this._guild) throw new Error("Guild not found!");

    // Get all non-bot and non-excluded members in the server
    let members = this._guild.members.cache.filter((user: GuildMember) => {
      // Exclude members that should be
      if(this._excluded.includes(user.user.id)) return false;
      // Exclude all bots
      return user.user.bot == false;
    });
    if(!members) throw new Error("Could not find members!");

    
    // Loop over each user to calculate their score
    let promises = [];
    for(let member of members) {
      // Create a promise which processes the member
      await new Promise(async (resolve, reject) => {
        // Load our reputation entries
        let reputations = await ReputationsModel.findAll({
          where: {
            receiver: member[1].user.id
          }
        })

        // Calculate our reputation
        let reputation = calculateReputation(reputations).total;

        // Get the message data for the user
        let data = await MessagesModel.findOne({
          where: {
            Uid: member[1].user.id
          }
        });

        let messageData: any = {};
        if(data) messageData = JSON.parse(<string>data.get("data"));

        // Get the amount of days the user is member of the server
        let membershipStart = moment(member[1].joinedAt?.toISOString());
        let today = moment();
        let membershipDays = today.diff(membershipStart, 'days');

        // Calculate the user's score
        let score = calculateScore(reputation, messageData.total, membershipDays);
        updatedLeaderboard.boards.push({
          user: member[1], 
          score: {
            messages: messageData.total,
            reputation: reputation,
            time: membershipStart,
            total: score
          }
        });
        resolve();
      }).catch((reason) => {
        console.log(reason);
      });
    }

    // Sort the board
    updatedLeaderboard.boards.sort((a: any, b: any) => {
      return b.score.total - a.score.total;
    });

    // Set the update time
    updatedLeaderboard.updatedAt = moment();

    // Swap out the leaderboards
    Leaderboard._leaderboard = updatedLeaderboard;

    console.log('Leaderboards updated!');
  }

  public static get() {
    return Leaderboard._leaderboard;
  }
}