import { MaidBot } from '../main';
import { Leaderboard } from '../Util/Leaderboard';

export class LeaderboardCron {
  public delay: number = 1200 * 1000;

  public job() {
    let leaderboard = new Leaderboard(MaidBot.getClient());
    leaderboard.update();
  }
}