import { LeaderboardCron } from './Cron/LeaderboardCron';

export class Cron {
  private _jobs: any[] = [];
  private _classes: any = {
    LeaderboardCron
  }

  public constructor() {
    this.registerJob('Leaderboard');
  }

  private registerJob(cronjob: string) {
    // Check if the class exists
    if(!this._classes.hasOwnProperty(`${cronjob}Cron`)) return;

    // Create a new instance from the class
    var instance = new this._classes[`${cronjob}Cron`]();

    // Register the job
    this._jobs.push(setInterval(instance.job, instance.delay));
  }

  public getJobs() {
    return this._jobs;
  }
}