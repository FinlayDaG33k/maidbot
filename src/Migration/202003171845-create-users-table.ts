import { Sequelize, QueryInterface, DataTypes  } from 'sequelize';

export = {
  up: (queryInterface: QueryInterface, sequelize: Sequelize) => {
    return queryInterface.createTable('Users', {
      uid: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },
      nickname: DataTypes.STRING
    });
  }
}