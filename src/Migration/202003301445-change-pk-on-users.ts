import { Sequelize, QueryInterface, DataTypes  } from 'sequelize';

export = {
  up: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    try {
      // Drop the Users table
      await queryInterface.sequelize.query(`PRAGMA foreign_keys=off;`);
      await queryInterface.dropTable('Users');
      await queryInterface.sequelize.query(`PRAGMA foreign_keys=on;`);

      // Recreate the Users table
      await queryInterface.createTable('Users', {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true
        },
        uid: DataTypes.STRING,
        nickname: DataTypes.STRING
      });

      // Update the Reputations table
      await queryInterface.changeColumn('Reputations', 'receiver', {
        type: DataTypes.STRING
      });
      await queryInterface.changeColumn('Reputations', 'giver', {
        type: DataTypes.STRING
      })

      // Update the messages table
      await queryInterface.changeColumn('Messages', 'uid', {
        type: DataTypes.STRING
      });
      
      return Promise.resolve();
    } catch(e) {
      return Promise.reject(e);
    }
  }
}