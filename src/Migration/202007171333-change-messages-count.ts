import { Sequelize, QueryInterface, DataTypes  } from 'sequelize';

export = {
  up: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    // Remove the old count
    await queryInterface.removeColumn('Messages', 'count');

    // Add data column
    await queryInterface.addColumn('Messages', 'data', {
      type: DataTypes.TEXT
    });
  }
}