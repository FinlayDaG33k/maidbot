import { Sequelize, QueryInterface, DataTypes  } from 'sequelize';

export = {
  up: (queryInterface: QueryInterface, sequelize: Sequelize) => {
    return queryInterface.createTable('Reputations', {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      receiver: {
        type: DataTypes.INTEGER,
        references: { model: 'Users', key: 'uid' }
      },
      giver: {
        type: DataTypes.INTEGER,
        references: { model: 'Users', key: 'uid' }
      },
      rep: DataTypes.STRING(1),
      message: DataTypes.STRING,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE
    });
  }
}