import { Sequelize, QueryInterface, DataTypes  } from 'sequelize';

export = {
  up: (queryInterface: QueryInterface, sequelize: Sequelize) => {
    return queryInterface.createTable('Messages', {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },
      uid: {
        type: DataTypes.INTEGER
      },
      count: DataTypes.INTEGER
    });
  }
}