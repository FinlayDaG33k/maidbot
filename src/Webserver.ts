import * as express from 'express';
import * as path from 'path';

export class Webserver {
  private directory: string = path.join(__dirname, '../docs');
  private port: number;
  private express: any;

  public constructor(port: number = 8080) {
    // Set the port
    this.port = port;

    // Instanciate express
    this.express = express();
    this.express.use(express.static(this.directory));
  }

  public start() {
    this.express.listen(this.port, () => {
      console.log(`Webserver is running on port ${this.port}`);
    })
  }
}