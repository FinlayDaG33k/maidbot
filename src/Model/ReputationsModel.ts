import { Database } from '../Database';
import { Model, DataTypes } from 'sequelize'
import { UsersModel } from './UsersModel';

class ReputationsModel extends Model {}

let sequelize = Database.getConn();

ReputationsModel.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  receiver: {
    type: DataTypes.STRING,
    references: {
      model: UsersModel,
      key: 'uid'
    }
  },
  giver: {
    type: DataTypes.STRING,
    references: {
      model: UsersModel,
      key: 'uid'
    }
  },
  rep: DataTypes.STRING(1),
  message: DataTypes.STRING,
  createdAt: DataTypes.DATE,
  updatedAt: DataTypes.DATE
}, {
  sequelize,
  modelName: 'Reputations'
});

export { ReputationsModel }