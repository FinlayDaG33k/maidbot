import { Database } from '../Database';
import { Model, DataTypes } from 'sequelize'
import { UsersModel } from './UsersModel';

class MessagesModel extends Model {}

let sequelize = Database.getConn();

MessagesModel.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true
  },
  uid: {
    type: DataTypes.STRING,
    references: {
      model: UsersModel,
      key: 'uid'
    }
  },
  data: DataTypes.TEXT
}, {
  sequelize,
  modelName: 'Messages',
  timestamps: false
});

export { MessagesModel }