import { Database } from '../Database';
import { Model, DataTypes } from 'sequelize'

class UsersModel extends Model {}

let sequelize = Database.getConn();

UsersModel.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true
  },
  uid: DataTypes.STRING,
  nickname: DataTypes.STRING
}, {
  sequelize,
  modelName: 'Users',
  timestamps: false
});

export { UsersModel }