export class Configure {
  private static _config: any = require('../config.json');

  /**
   * Read a value from the configs.
   * 
   * @param key 
   */
  public static read(key: string): any {
    return this._config[key];
  }

  /**
   * Write a value to the configs.
   * Changes made using this method do not persist.
   * 
   * @param key 
   * @param value 
   */
  public static write(key: string, value: any): any {
    return this._config[key] = value;
  }

  /**
   * Write out the config to the config file to persist.
   */
  public static save() {
    // TODO: Implement this method
  }
}