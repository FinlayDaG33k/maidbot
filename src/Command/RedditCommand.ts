import { Message } from "discord.js";

export class RedditCommand {
  private _message: Message;

  public constructor(data: any = {}) {
    this._message = data.message;
  }

  public run(args: any = {}) {
    this._message.reply("You can find Teitoku's reddit at `r/FinlayDaG33k`: https://www.reddit.com/r/FinlayDaG33k/");
  }
}