import { Message, Client } from "discord.js";
import { getArguments } from '../Util/getArguments';
import { ReputationsModel } from '../Model/ReputationsModel';
import { UsersModel } from '../Model/UsersModel';

export class RepCommand {
  private _message: Message;
  private _client: Client;
  private _args: any = [];

  public constructor(data: any = {}) {
    this._message = data.message;
    this._client = data.client;
    this._args = getArguments(data.message.content, 3);
  }

  public async run() {
    // Check if arguments have been specified
    if(this._args.length < 2) {
      this._message.reply("Please mention the user you want to rep");
      return;
    }

    // Check if a receiver has been mentioned
    if(!this._args[1].startsWith('<@') || !this._args[1].endsWith('>')) {
      this._message.reply("Please mention the user you want to rep");
      return;
    }

    // Get the receiver object
    let receiver = this._args[1].slice(3,-1);
    receiver = await this._message.client.users.fetch(receiver);
    if(!receiver) throw new Error("Receiver could not be obtained!");

    // Check if the receiver is self
    if(receiver.id == this._message.author.id) {
      this._message.reply("I'm afraid you can't rep yourself Master.");
      return;
    }

    // Check if a rep has been specified
    if(!this._args[2]) {
      this._message.reply("Master, please specify whether you want to give a positive rep (`+`) or a negative rep (`-`)");
      return;
    }

    if(!["+", "-"].includes(this._args[2])) {
      this._message.reply(`Master, I'm afraid a rep of "${this._args[2]}" won't do.\r\nPlease specify whether you want to give a positive rep (\`+\`) or a negative rep (\`-\`)`);
      return;
    }

    // Find whether we already have a rep between sender and receiver
    let res = await ReputationsModel.findOne({
      where: {
        receiver: receiver.id,
        giver: this._message.author.id
      }
    });

    // Create an object that will hold our data
    let rep = {
      receiver: receiver,
      giver: this._message.author,
      rep: this._args[2],
      message: this._args[3] || 'No reason given'
    };

    // Check whether a rep has been found
    // Update if found
    // Add if not found
    if(res) {
      await this.updateRep(res, rep);
    } else {
      await this.addRep(rep);
    }
  }

  public async updateRep(old: any, updated: any) {
    // Swap out some values
    updated.receiver = updated.receiver.id;
    updated.giver = updated.giver.id;

    try {
      await old.update(updated);

      // Check if we are the recipient
      if(updated.receiver != this._client.user?.id) {
        this._message.reply(`Master, I have successfully updated the reputation you requested me to do!`);
        return;
      }

      // We are the recipient
      // Check whether the rep was positive or negative
      switch(updated.rep){
        case '+':
          this._message.reply(`It makes me happy to hear you like my service.\r\nThank you, Master.`);
          break;
        case '-':
          this._message.reply(`It makes me sad to hear you don't like my service Master.\r\nI will continue to work on becoming a better maid!`);
          break;
      }
    } catch (e) {
      console.error(e);
      this._message.reply("Master, I'm afraid something went wrong while trying to update the reputation you requested me to do...\r\nWould you mind to please try again later?");
    }
  }

  public async addRep(rep: any) {
    // Make sure both users exist
    await UsersModel.findOrCreate({
      where: {
        uid: rep.receiver.id,
        nickname: rep.receiver.username
      }
    });

    await UsersModel.findOrCreate({
      where: {
        uid: rep.giver.id,
        nickname: rep.giver.username
      }
    });

    // Swap out some values
    rep.receiver = rep.receiver.id;
    rep.giver = rep.giver.id;

    // Insert the rep
    try {
      await ReputationsModel.create(rep);
      // Check if we are the recipient
      if(rep.receiver.id != this._client.user?.id) {
        // We are not the recipient
        this._message.reply(`Master, I have successfully added the reputation you requested me to do!`);
        return;
      }

      // We are the recipient
      // Check whether the rep was positive or negative
      switch(rep.rep){
        case '+':
          this._message.reply(`It makes me happy to hear you like my service.\r\nThank you, Master.`);
          break;
        case '-':
          this._message.reply(`It makes me sad to hear you don't like my service Master.\r\nI will continue to work on becoming a better maid!`);
          break;
      }
      
    } catch (e) {
      console.error(e);
      this._message.reply("Master, I'm afraid something went wrong while trying to add the reputation you requested me to do...\r\nWould you mind to please try again later?");
    }
  }
}