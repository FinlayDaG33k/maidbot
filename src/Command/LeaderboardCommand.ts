import { Message, MessageEmbed } from "discord.js";
import { Leaderboard } from '../Util/Leaderboard';

export class LeaderboardCommand {
  private _message: Message;
  private _embed: MessageEmbed = new MessageEmbed();
  private _leaderboard = Leaderboard.get();

  public constructor(data: any = {}) {
    this._message = data.message;
  }

  public run() {
    // Build our embed
    this._embed.setColor('#002047');
    this._embed.setTitle('Das Propaganda Maschine Leaderboard')
    this.formatBoard();
  
    // Send the embed
    this._message.reply(this._embed);
  }

  private formatBoard() {
    // Get the first 10 places
    let items = this._leaderboard.boards.slice(0, 10);

    // Variable to hold our board
    let board = '';

    // Add our entries
    let place: number = 1;
    for(let entry of items) {
      this._embed.addField(`${place}. ${entry.user.user.username}`, `
      **Reputation:** ${entry.score.reputation}
      **Messages:** ${entry.score.messages}
      **Joined:** ${entry.score.time.format('MMM Do, GGGG')}
      **Score:** ${entry.score.total}
      `, true);

      // Increase the place
      place++;
    }

    this._embed.setFooter(`Last update: ${this._leaderboard.updatedAt}\r\nNext update: ${this._leaderboard.updatedAt.add(30, 'm')}`)
  }
}