import { Message, Role } from "discord.js";
import { getArguments } from '../Util/getArguments';

export class RoleCommand {
  private _message: Message;
  private _args: any = {};
  private _selfAssignable: string[] = [
    "VOC"
  ];

  public constructor(data: any = {}) {
    this._message = data.message;
    this._args = getArguments(data.message.content, 1);
  }

  public async run() {
    // Check if a guild is defined
    if(!this._message.guild) throw new Error("Guild not defined!");

    // Check if a role has been specified
    if(!this._args[1]) {
      this._message.reply("Please tell me what role you'd like me to assign, Master");
    }

    // Check if we are allowed to assign this role through the bot
    if(!this._selfAssignable.includes(this._args[1])) {
      // We're not allowed to assign this role
      // Show an error message
      let message = "Master, I'm afraid you are only allowed to assign public roles!\r\nI have made a small list of such roles you you can pick one you want:";
      message += "```";
      this._selfAssignable.forEach((role: string) => {
        message += `- ${role}\r\n`;
      });
      message += "```";
      this._message.reply(message);
      return;
    }

    // Get a list of all roles
    const roles = await this._message.guild.roles.fetch();

    // Check if the requestes role exists
    let role = roles.cache.find((r: Role) => {
      return r.name == this._args[1];
    });

    if(!role) {
      this._message.reply("The requested role does not exist on this server");
      return;
    }

    // Assign the user to the role
    try {
      this._message.member?.roles.add(role);
      this._message.reply(`I have assigned you to the requested role "${role.name}".\r\nIf there are any more roles you'd like me to assign, please let me know.`);
    } catch (e) {
      console.error(e);
      this._message.reply(`Master, I'm afraid something went wrong while trying to assign this role.\r\nWould you mind to please try again later?`);
    }
  }
}