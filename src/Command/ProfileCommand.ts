import { Message, Client, MessageEmbed, GuildMember, Channel } from "discord.js";
import { getArguments } from '../Util/getArguments';
import { ReputationsModel } from '../Model/ReputationsModel';
import * as moment from 'moment';
import { MessagesModel } from "../Model/MessagesModel";
import { calculateScore } from '../Util/calculateScore';
import { giveawayPower } from '../Util/giveawayPower';
import { calculateReputation } from '../Util/calculateReputation';

export class ProfileCommand {
  private _message: Message;
  private _client: Client;
  private _args: any = [];
  private _data: any = {};
  private _user: GuildMember | undefined | null;

  public constructor(data: any = {}) {
    this._message = data.message;
    this._client = data.client;
    this._args = getArguments(data.message.content, 3);
  }

  public async run(args: any = {}) {
    // Check if we mentioned a user in specific or not
    if(this._args.length < 2) {
      // No user mentioned
      // Use the issuer
      this._user = this._message.member;
    }  else {
      // User mentioned
      // Use it
      this._user = this._message.mentions.members?.first();
    }

    // Check if we've set a user
    if(!this._user) {
      this._message.reply("I must apologise but it appears something went wrong while executing this command.\r\nWould you mind trying again later?");
      return;
    }

    // Get the profile data
    await this.getProfile(this._user.id);

    // Create and send our reply
    this._message.channel.send(this.createReply());
  }

  private async getProfile(uid: any) {
    // Get our reputation
    let reps = await ReputationsModel.findAll({
      where: {
        'receiver': uid
      }
    });

    this._data.reputation = calculateReputation(reps);

    // Get the messages send
    let data = await MessagesModel.findOne({
      where: {
        uid: uid
      }
    });

    if(!data) {
      this._data.messages.total = 0;
    } else {
      this._data.messages = JSON.parse(<string>data.get("data"));
    }

    // Get the amount of days the user is member of the server
    let membershipStart = moment(this._user?.joinedAt?.toISOString());
    let today = moment();
    let membershipDays = today.diff(membershipStart, 'days');

    // Get the user's score
    this._data.score = calculateScore(this._data.reputation.score, this._data.messages.total, membershipDays);

    // Get the user's avatar url
    this._data.avatarURL = this._user?.user.displayAvatarURL();

    // Get the user's favourite channel
    let channels = Object.keys(this._data.messages.channels).sort((a,b) => { return this._data.messages.channels[b] - this._data.messages.channels[a] }).splice(0, 3);
    this._data.favouriteChannels = ``;
    for(let channel of channels) {
      this._data.favouriteChannels += `<#${channel}> (${this._data.messages.channels[channel]})\r\n`;
    }
  }

  private createReply() {
    let joinedAt = moment(this._user?.joinedAt?.toISOString());
    let lastSeen = moment(this._user?.lastMessage?.createdAt.toISOString());
    let repPercentage = (this._data.reputation.upvotes / this._data.reputation.total) * 100;
    if(isNaN(repPercentage)) repPercentage = 0;

    const embed = new MessageEmbed()
      .setColor('#002047')
      .setTitle(`Profile of ${this._user?.user.username}`)
      .setThumbnail(this._data.avatarURL)
      .setDescription(`
      **Reputation:** ${this._data.reputation.score} (${repPercentage}%)
      **Messages:** ${this._data.messages.total}
      **Score:** ${this._data.score}
      **Giveaway Power**: ${giveawayPower(this._data.score)}
      `)
      .addField('Favourite Channels:', `${this._data.favouriteChannels}`)
      .addField('First Seen:', `${joinedAt.format("MMM Do, GGGG")} at ${joinedAt.format("HH:mm")}`, true)
      .addField('last Seen:', `${lastSeen.format("MMM Do, GGGG")} at ${lastSeen.format("HH:mm")}`, true);

    return embed;
  }
}