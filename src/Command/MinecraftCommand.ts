import { Message } from "discord.js";
import { Configure } from '../Configure';
import { getArguments } from '../Util/getArguments';
import { Rcon } from 'rcon-client';

export class MinecraftCommand {
  private _message: Message;
  private _args: any = [];

  public constructor(data: any = {}) {
    this._message = data.message;
    this._args = getArguments(data.message.content, 3);
  }

  public run(args: any = {}) {
    // Check if a specific command has been ran
    if(this._args.length < 2) {
      this._message.reply(`If you are unsure what to enter next, please visit the wiki:\r\n${Configure.read("wiki")}/commands/Minecraft`);
      return;
    }

    switch(this._args[1]) {
      case 'whitelist':
        this.whitelistUser();
        break;
      default:
        this._message.reply(`I'm unsure what you want me to do...\r\n"Minecraft" has no command named "${this._args[1]}"...`);
        break;
    }
  }

  /**
   * Whitelists a specific user
   * 
   * @returns Promise<void>
   */
  private async whitelistUser(): Promise<void> {
    // Check if a user has been specified
    if(this._args.length < 3) {
      this._message.reply(`Who do you want me to whitelist, Master?`);
      return;
    }

    const rcon = await Rcon.connect(Configure.read("minecraft-rcon"));
    try {
      await rcon.send(`whitelist add ${this._args[2]}`);
      this._message.reply(`Master, I've added "${this._args[2]}" to the whitelist!\r\nYou can join the server using the following address: \`${Configure.read("minecraft-host")}:${Configure.read("minecraft-port")}\`\r\nA Dynmap is also available here:\r\nhttps://${Configure.read("minecraft-host")}\r\n\r\nEnjoy playing!`);
    } catch(e) {
      this._message.reply(`It seems that something went wrong while trying to add "${this._args[2]}" to the whitelist.\r\nPlease try again later.`);
    }

    rcon.end();
  }
}