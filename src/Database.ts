import { Sequelize } from 'sequelize';
import { Configure } from './Configure';
import * as Umzug from 'umzug';
import * as path from 'path';

export class Database {
  private static _conn: Sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: Configure.read('database')
  });

  public static async open() {
    return this._conn
      .authenticate()
      .then(() => {
        console.log("Connection to the database has been established successfully");
      })
      .catch((err) => {
        console.error("Could not connect to the database: " + err);
      });
  }

  public static async migrate() {
    const umzug = new Umzug({
      migrations: {
        path: path.join(__dirname, './Migration'),
        params: [
          this._conn.getQueryInterface()
        ]
      },
      storage: 'sequelize',
      storageOptions: {
        sequelize: this._conn
      }
    });

    // checks migrations and run them if they are not already applied
    await umzug.up()
    console.log('All migrations performed successfully');

    return;
  }

  public static getConn() {
    return this._conn;
  }
}