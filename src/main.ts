import { Message, GuildMember, MessageEmbed, Role } from 'discord.js';
import { Client, On } from '@typeit/discord';
import { Configure } from './Configure';
import { Database } from './Database';
import { Webserver } from './Webserver';

import { UsersModel } from './Model/UsersModel';

import * as Command from './Command';
import { MessageCount } from './Util/MessageCount';
import { ScanUsers } from './Util/ScanUsers';
import { Leaderboard } from './Util/Leaderboard';
import { Cron } from './Cron';

export class MaidBot {
  private static _client: Client;
  private static _prefix: string = "!";
  private static _commands: any = Command;
  private static _mentionReplies: any = [
    "Yes, Master?",
    "Is there anything I can help you with, Master?",
    "Did you call for me, Master?",
    "You have summoned me, Master?"
  ]
  private static _webserver: Webserver;
  private static _messageCount: MessageCount = new MessageCount();
  private static _cron: Cron;

  /**
   * Initialize and start the Discord bot
   */
  public static async start() {
    // Open a connection to the database
    // Run the migrations afterwards
    await Database.open();
    await Database.migrate();

    // Start our webserver
    this._webserver = new Webserver();
    this._webserver.start();

    // Initialize the client
    this._client = new Client();

    // Login with our token
    await this._client.login(Configure.read('token'));
  }

  @On("ready")
  async onReady() {
    // Scan all users in the server
    let scanUsers = new ScanUsers(MaidBot._client);
    await scanUsers.scan();

    // Scan all messages we might have missed
    await MaidBot._messageCount.scan(MaidBot._client);

    // Update the leaderboard
    let leaderboard = new Leaderboard(MaidBot._client);
    await leaderboard.update();

    // Start our cronjobs
    MaidBot._cron = new Cron();
  }

  /**
   * Event for when a message has been send
   * 
   * @param message 
   * @param client 
   */
  @On("message")
  async onMessage(message: Message, client: Client) {
    // Make sure we have are logged in
    if(!MaidBot._client.user) return;

    // Increase the message count
    MaidBot._messageCount.increase(message.author.id);

    // Make sure we are not the senders (to prevent loops)
    if(MaidBot._client.user.id == message.author.id) return;

    // Check if we are being mentioned
    if(message.content.startsWith('<@') && message.content.endsWith('>')) {
      if(message.mentions.members?.first()?.id != MaidBot._client.user.id) return;

      message.reply(MaidBot._mentionReplies[Math.floor(Math.random() * MaidBot._mentionReplies.length)]);
      return;
    }

    // Check if the message has the correct prefix
    if(message.content[0] !== MaidBot._prefix) return;

    // Get the command string
    // Remove the prefix
    // Get the first word
    // Transform to all lowercase
    // Uppercase the first letter
    let cmd = message.content;
    cmd = cmd.replace(MaidBot._prefix, "");
    cmd = cmd.split(" ")[0];
    cmd = cmd.toLowerCase();
    cmd = cmd[0].toUpperCase() + cmd.slice(1);
    
    try {
      // Make sure it's a valid command
      // Ignore it otherwise (there might be another bot handling it)
      if(!MaidBot._commands.hasOwnProperty(`${cmd}Command`)) return;

      // Create a data object to pass to the command
      let data = {
        message: message,
        client: MaidBot._client
      };

      // Instanciate a class and run the command
      new MaidBot._commands[`${cmd}Command`](data).run();
    } catch(e) {
      // Send a message if something went wrong
      message.reply("I'm afraid something went wrong while executing the command...\r\nWould you mind to please try again later?");
      console.error(e);
    }
  }

  @On("guildMemberAdd")
  async onGuildMemberAdd(member: GuildMember) {
    // Create some informational embeds
    const embed = new MessageEmbed()
      .setColor('#002047')
      .setTitle("Welcome to Das Propaganda Machine!")
      .setDescription(`Hello Master,
      My name is MaidBot and on behalf of Teitoku, I want to welcome you to Das Propaganda Maschine.
      I have been created by Teitoku specifically for DPM to help people with all kinds of stuff.
      If you want to see what I can do, please visit my [wiki](${Configure.read("wiki")}).
      Feel free to call me at any time and I'll help you to the best of my ability, but I'm still very young and learning, so please don't be angry at me if I make a mistake.
  
      Before you get started on DPM, I want you to know that Teitoku has specifically chosen too keep the rules the same as on his [subreddit](${Configure.read("reddit")}).
      The server's purpose is to have a server you can have fun and discuss all kinds of things, just like his subreddit, just more realtime.
      Please, don't feel shy to say anything on the server, we'd love to have the server bloom!

      I hope you will enjoy my service and your stay in Das Propaganda Maschine!
      `)
      .addField(`Teitoku's Discord Tag`, Configure.read("discord tag"), true)
      .addField(`Teitoku's Website`, Configure.read("website"), true)
      .addField(`Teitoku's Livestream`, Configure.read("livestream"), true)

    // Send our message
    member.user.send(embed);

    // Try to add the user to our database
    await UsersModel.findOrCreate({
      where: {
        uid: member.user.id,
        nickname: member.user.username
      }
    });

    // Get a list of all roles
    const roles = await member.guild.roles.fetch();

    // Check if the ensign role exists
    let role = roles.cache.find((r: Role) => {
      return r.name == 'Ensign'
    });

    if(!role) return;

    // Assign the role
    member.roles.add(role);
  }

  public static getClient() {
    return MaidBot._client;
  }
}

MaidBot.start();
